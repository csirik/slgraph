#!/usr/bin/python3


import numpy as np
from poly import *
from quad import *
from fe1 import *

np.set_printoptions(precision=4, linewidth=120,suppress=True)


Aloc, Mloc = lgl_stiff_mass(2)

print(Aloc)
print(Mloc)
