#!/usr/bin/python3

from poly import *
from quad import *

def test_quad(xw, Pmax):
    err = []
    for n in range(Pmax+1):
        xn = mon(2*n)
        exact = pi*factorial(2*n) /(4**n * factorial(n)**2)
        err.append( fabs(quadp(xn, -1, 1, xw) - exact) )

    return max(err)


xw = xw_cheby_gauss(10)

print("xw=" + str(xw))

print(test_quad(xw,10))

