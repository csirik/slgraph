#!/usr/bin/python3

import matplotlib.pyplot as plt
from poly import *
from quad import *

tol = 1e-13


def asseq(x,y):
    assert( fabs(x-y)<= tol )

def test_monomials(Kmax, XW):
    for K in range(Kmax):
        # x^K
        P = np.zeros(K + 1)
        P[0] = 1
        exact = 1 / (K + 1)

        asseq(xw_quad(P, 0, 1, XW), exact)


print("Testing quadrature rules...")
np.set_printoptions(precision=4, linewidth=120,suppress=True)

Nmax = 100

if False:
    print(">> Legendre-Gauss quadrature...")

    for N in range(2,Nmax):
        ab = r_jacobi(N, 0, 0)

        XW = xw_gauss( N, ab )

        test_monomials(2*N-3, XW)

    print("OK")


if True:
    print(">> Legendre-Gauss-Lobatto quadrature...")

    for N in range(2,Nmax):
        ab = r_jacobi(N+2, 0, 0)

        XW = xw_lobatto( N, ab, -1, 1 )

        test_monomials(2*N-3, XW)

    print("OK")



