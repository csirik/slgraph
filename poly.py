#!/usr/bin/python3

from matrix_utils import *
from recurrence import r_jacobi
import copy

def poly(L):
    return matrix(L).T

# Convert recurrence coefficients to polynomials
def ab_to_poly(ab):
    n = ab.rows
    P = zeros(n)
    P[0,n-1] = mpf(1)
    P[1,:] = row_join(zeros(1,n-2), matrix([mpf(1), -ab[0,0]]).T)
    for k in range(2,n+1):
        P[k-1,:] = row_join(P[k-2,1:n],matrix([mpf(0)])) - P[k-2,:]*ab[k-2,0] - ab[k-2,1]*P[k-3,:]
    return P

# Convert general recurrence coefficents to polynomials
def r_to_poly(B):
    n = B.rows
    P = zeros(n)
    P[0,n-1] = mpf(1)

    for k in range(n-1):
        S = zeros(1,n)
        for j in range(k+1):
            S = S + B[j,k]*P[k-j,:]
        tpik = row_join(P[k,:], zeros(1))
        P[k+1,:] = tpik[1:n+1] - S
    return P

# Evaluate polynomial on a vector of points (TRACE)
def polyvalv(p,xx):
    yy = zeros(1,len(xx))
    i = 0
    for x in xx:
        yy[i] = polyval(p,x)
        i += 1
    return yy

def polyvalres(p,a,b,res):
    xx = linspace(a,b,res)
    yy = polyvalv(p,xx)
    return xx,yy

# Polynomial axpy
def polyaxpy(a,x,y):
    dx,dy = len(x),len(y)
    if dx < dy:
        z = row_join(zeros(1,dy-dx), a*x)+y
    else:
        z = a*x+row_join(zeros(1,dx-dy), y)
    return z

# Discrete convolution
# From http://www.physics.rutgers.edu/~masud/computing/WPark_recipes_in_python.html
def conv(x, y):
    P, Q, N = len(x), len(y), len(x)+len(y)-1
    z = zeros(1,N)
    for k in range(N):
        t, lower, upper = 0, max(0, k-(Q-1)), min(P-1, k)
        for i in range(lower, upper+1):
            t = t + x[i] * y[k-i]
            z[k] = t
    return z

# Convolution of multiple polynomials (represented as rows of a matrix P) with a fixed one, p.
def convm(P, p):
    Q = zeros(P.rows, P.cols+len(p)-1)
    for i in range(P.rows):
        Q[i,:] = conv(P[i,:],p)
    return Q

# Convenience routine for monomial x^n
def mon(n):
    p = matrix(1,n+1)
    p[0] = mpf(1)
    return p

# Formal polynomial derivative
def polyder(p):
    N = len(p)
    return [ mpf(N-1-i)*p[i] for i in range(0,N-1) ]

# Formal polynomial primitive function with constant term zero
def polypri(p):
    N = len(p)
    q = row_join(p, zeros(1))
    for i in range(0,N): q[i] /= mpf(N-i)
    return q

# Evaluate the definite integral of the polynomial p using quadrature rules xw
def quadp(p, a, b, xw):
    paff,jaff = polyaffj(p,mpf(-1),mpf(1),a,b)
    return jaff*fdot(polyvalv(paff,xw[:,0]), xw[:,1])

# Evaluate the definite integral of the polynomial pq using quadrature rules xw
def quadpq(p, q, xw):
    return fdot(mult(polyvalv(p,xw[:,0]),polyvalv(q,xw[:,0])),xw[:,1])

# Evaluate the definite integral of the polynomial pq on an
# arbitrary interval [a,b] using the quadrature rules xw given on [-1,1]
def quadpqab(p, q, a, b, xw):
    paff,jaff = polyaffj(p,mpf(-1),mpf(1),a,b)
    qaff,jaff = polyaffj(q,mpf(-1),mpf(1),a,b)
    return jaff*fdot(mult(polyvalv(paff,xw[:,0]), polyvalv(qaff,xw[:,0])), xw[:,1])
    
# Compute composition p(s(x))
def polycomp(p, s):
    dp,ds = len(p)-1,len(s)-1 # deg(p),deg(s)
    dps = dp*ds # deg(p(s(x)))=deg(p)deg(s)
    q = zeros(1,dps+1) 
    r = ones(1) 
    for k in range(0,dp+1):
        q = q + row_join(zeros(1,dps-k*ds), p[dp-k]*r) # deg(r)=k*deg(s)
        r = conv(r,s) # r=s^k
    return q

# Compose polynomial p with the affine transform [a,b]->[c,d]
def polyaff(p, a, b, c, d):
    return polycomp(p, matrix([(c-d)/(a-b), (a*d-b*c)/(a-b)]))

## def polyaff0(p, a, b, c, d):
##     aff = matrix([(c-d)/(a-b), (a*d-b*c)/(a-b)])
##     n = len(p)
##     q = row_join(zeros(1,n-1), matrix([p[n-1]]))
##     r = matrix([mpf(1)])
##     for k in range(n-1,0,-1):
##         r = conv(r,aff)
##         q = q + row_join(zeros(1,k-1), p[k-1]*r);
##     return q

# Same as polyaff but also returns the Jacobian of the transform
def polyaffj(p, a, b, c, d):
    return polyaff(p, a, b, c, d), ((c-d)/(a-b))

# affine transform quadrature rule
def xwaff(xw,a,b,c,d):
    xw1 = copy.copy(xw)
    for i in range(xw.rows):
        xw1[i,0] = xw[i,0]*(c-d)/(a-b)+(a*d-b*c)/(a-b)
        xw1[i,1] = xw[i,1]*(c-d)/(a-b)
    return xw1

# Calculate L2 norm squared using quadrature rules xw
def polyl2(p, xw):
    return quadpq(p,p,xw)

# Calculate L2 norm squared using quadrature rules xw on the interval [a,b]
# The quadrature rules are assumed to have support [-1,1]
def polyl2ab(p, a, b, xw):
    paff = polyaff(p,mpf(-1),mpf(1),a,b)
    return quadpq(paff,paff,xw)

# L2 norm squared
def l1norm2(p,xw):
    return quadpq(p,p,xw)

# L2 norm squared on [a,b]
def l1norm2ab(p,a,b,xw):
    return quadapq(p,p,a,b,xw)

# H1 inner product k*\int pq+l*\int p'q'
def h1inner(p,q,xw,k0=mpf(1),k1=mpf(1)):
    i0 = quadpq(p,q,xw)
    i1 = quadpq(polyder(p),polyder(q),xw)
    return k0*i0+k1*i1

# H1 inner product k*\int pq+l*\int p'q' on [a,b]
def h1innerab(p,q,a,b,xw,k0=mpf(1),k1=mpf(1)):
    i0 = quadpqab(p,q,a,b,xw)
    i1 = quadpqab(polyder(p),polyder(q),a,b,xw)
    return k0*i0+k1*i1

# H1 norm squared
def h1norm2(p,xw,k=mpf(1),l=mpf(1)):
    return h1inner(p,p,xw,k,l)

# H1 norm squared on [a,b]
def h1norm2ab(p,a,b,xw,k=mpf(1),l=mpf(1)):
    return h1innerab(p,p,a,b,xw,k,l)

# Chebyshev nodes on [-1,1]
def cheby1(n):
    return matrix([ cos(pi*mpf(2*k-1)/mpf(2*n)) for k in range(n,0,-1) ])

# Extended Chebyshev nodes of the second kind on [-1,1]
def cheby2(n):
    return matrix([ cos(pi*mpf(n-k)/mpf(n)) for k in range(0,n+1) ])


# Compute Lagrange basis polynomials on nodes X
def lagrange(X):
    n = len(X)
    if n < 1:
        raise(ValueError, "Invalid nodes")

    L = zeros(n,n)
    for j in range(n):
        Lj = [mpf(1)]
        denom = mpf(1)
        for k in range(n):
            if k != j:
                Lj = conv(Lj, [mpf(1), -X[k]])
                denom *= X[j]-X[k]
        L[j,:] = Lj/denom

    return L

# compute lobatto polinomials
def lobatto(n):
    ab = r_jacobi(n,0,0)
    leg = ab_to_poly(ab)
    lob = zeros(n+1)
    lob[0,:] = row_join(zeros(1,n-1), matrix([[mpf(-0.5), mpf(0.5)]]))
    lob[1,:] = row_join(zeros(1,n-1), matrix([[mpf(0.5), mpf(0.5)]]))

    for k in range(2,n):
        leg[k-1,:] /= polyval(leg[k-1,:], 1)
        lob[k,:] = polypri(leg[k-1,:])
        lob[k,n] = -polyval(lob[k,:],-1)
        lob[k,:] *= sqrt(mpf(2*k-1)/mpf(2))
    
    return lob


# evaluate nth (alpha,beta)-jacobi polynomial at x
def jacobi_eval(x,n,alpha,beta):
#% JACOBI_EVAL: Evaluates Jacobi polynomial P_n^{(\alpha,\beta)} at x\in(-1,1)
#%
#%              (formula (2.5.3) pag. 92, CHQZ2)
#%  [p,pd] = jacobi_eval(x,n,alpha,beta) 
#%
#% Input: x = scalar or one-dimensional array of length (m) 
#%        n =  polynomial degree
#%        alpha, beta= parameters of Jacoby polynomial
#%
#% Output: p(m,3) = [P_n^{(\alpha,\beta)}(x),
#%                   P_(n-1)^{(\alpha,\beta)}(x),
#%                   P_(n-2)^{(\alpha,\beta)}(x)];
#%
#%         pd(m,3) = [(P_n^{(\alpha,\beta)})'(x),
#%                    (P_(n-1)^{(\alpha,\beta)})'(x),
#%                    (P_(n-2)^{(\alpha,\beta)})'(x)];
#%
#% Reference: CHQZ2 = C. Canuto, M.Y. Hussaini, A. Quarteroni, T.A. Zang,
#%                    "Spectral Methods. Fundamentals in Single Domains"
#%                    Springer Verlag, Berlin Heidelberg New York, 2006.
#%   Written by Paola Gervasio
#%   $Date: 2007/04/01$

    apb = alpha + beta
    ab2 = alpha**2 - beta**2

    nn = x.rows
    if nn == 1:

        # x is a scalar
        x = x[0]
        p = 1
        pd = 0
        p1 = 0
        pd1 = 0
        p2 = 0
        pd2 = 0
        if n == 0:
            return
        elif n == 1:
            p1 = p
            p2 = p1
            p = (alpha-beta+(apb+2)*x)*0.5
            pd1 = pd
            pd2 = pd1
            pd = 0.5*(apb+2)
        else:
            p1 = p
            p2 = p1
            p = (alpha-beta+(apb+2)*x)*0.5

            pd1 = pd
            pd2 = pd1
            pd = 0.5*(apb+2)
        
            for k in range(1,n):
                k1 =k+1
                k2 = k*2.0
                k2ab= k2 + alpha+beta
                k2ab1 = k2ab+1
                k2ab2 = k2ab1+1
                p2 = p1
                p1 = p
                pd2 = pd1
                pd1 = pd
                a1 = 2.0*k1*(k1+apb)*k2ab
                # Gamma(x+n)/Gamma(x) = (x+n-1) * ... * (x+1) * x 
                a21 = k2ab1*ab2
                a22 = k2ab2*k2ab1*k2ab
                a3 = 2.0*(k+alpha)*(k+beta)*k2ab2
                p = ((a21+a22*x)*p1-a3*p2)/a1
                pd = (a22*p1+(a21+a22*x)*pd1-a3*pd2)/a1
    else:
        # x is an array
        m = x.rows
        p = row_join( row_join( ones(m,1), zeros(m,1)), zeros(m,1) )
        pd = zeros(m,3)
        
        if n == 0:    
            return 
        elif n == 1:
            p[:,1] = p[:,0]
            p[:,2] = p[:,1]
            p[:,0] = (alpha-beta+(apb+2)*x)*0.5; 

            pd[:,1] = pd[:,0]
            pd[:,2] = pd[:,1]
            pd[:,0] = 0.5*(apb+2) 
        else:
            p[:,1] = p[:,0]
            p[:,2] = p[:,1]
            p[:,0] = (alpha-beta+(apb+2)*x)*0.5

            pd[:,1] = pd[:,0]
            pd[:,2] = pd[:,1]
            pd[:,0] = 0.5*(apb+2)
            
            for k in range(1,n):
                k2 = k*2
                k2ab = k2+alpha+beta
                
                p[:,2] = p[:,1]
                p[:,1] = p[:,0]
                pd[:,2] = pd[:,1]
                pd[:,1] = pd[:,0] 
                a1 = 2*(k+1)*(k+apb+1)*k2ab
                # Gamma(x+n)/Gamma(x) = (x+n-1) * ... * (x+1) * x 
                a21 = (k2ab+1)*ab2
                a22 = (k2ab+2)*(k2ab+1)*k2ab
                a3 = 2*(k+alpha)*(k+beta)*(k2ab+2)
                
                p[:,0] = (mult(a21+a22*x, p[:,1])-a3*p[:,2])/a1
                pd[:,0] = (mult(a22*p[:,1]+(a21+a22*x),pd[:,1])-a3*pd[:,2])/a1
    return p, pd
 
 
def jacobi_roots(n,alpha,beta):
#% JACOBI_ROOTS: Computes the n zeros of the Jacoby polynomial P_n^{(\alpha,\beta)}(x)
#%   by Newton method and deflation process.
#%
#%    [x] = jacobi_roots(n,alpha,beta) 
#%
#% Input: n = polynomial degree
#%        alpha,beta = parameters of Jacobi polynomial
#%
#% Output: x = zeros of Jacobi polynomial (column array of size n)
#%         flag  = -1: The polynomial degree should be greater than 0
#%
#% Reference: CHQZ2 = C. Canuto, M.Y. Hussaini, A. Quarteroni, T.A. Zang,
#%                    "Spectral Methods. Fundamentals in Single Domains"
#%                    Springer Verlag, Berlin Heidelberg New York, 2006.

#%   Written by Paola Gervasio
#%   $Date: 2007/04/01$


    if n < 1:
        print('The polynomial degree should be greater than 0')
        return []
 
    else:
        x = zeros(n,1)

        x0 = mpf(cos(pi/(2.0*n)))
        tol = 1.e-14
        kmax = 100
        for j in range(n):
            diff=tol+1
            kiter=0
            while kiter <= kmax and diff >= tol:
                p,pd = jacobi_eval(matrix([x0]),n,alpha,beta)
                #% deflation process q(x)=p(x)*(x-x_1)*... (x-x_{j-1})
                #% q(x)/q'(x)=p(x)/[p'(x)-p(x)*\sum_{i<j} 1/(x-x_i)]
                ss = sum( div(ones(j,1), ones(j,1)*x0 - x[0:j,0]) )
                x1 = x0 - p/(pd-ss*p)
                diff = fabs(x1-x0)
                kiter = kiter + 1
                x0 = x1
                
            x0 = 0.5 * ( x1 + cos((2*(j+2)-1)*pi/(2*n)) )
            x[j] = x1
            
        x = sorted(x)
        return x


