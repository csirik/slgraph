#!/usr/bin/python3

import matplotlib.pyplot as plt
from chebyshev import *
from poly import *

print("Tesing orthogonal polynomials and quadrature rules...")


N = 10

if False:
    print(">> Chebyshev polynomials")
    chebab = r_jacobi(N, -0.5, -0.5)
    chebpoly = ab_to_poly(chebab)

    fig, ax = plt.subplots()
    for k in range(N):
        xx,yy = polyvalres(chebpoly[k,:], -1, 1, 100)
        ax.plot(xx,yy, '-')
        ax.set_aspect('equal')

        print(">> Chebyshev nodes...")

    yy = zeros(1,N-1)
    for k in range(1,N):
        yy[k-1] = max(polyvalv(chebpoly[k,:], cheby1(k)))

    print("N=" + str(N) + " maxerr=" + str(max(yy)))

    fig, ax = plt.subplots()

    Nplotcheby = 50

    for k in range(1,Nplotcheby):
        xx = cheby2(k)
        yy = ones(len(xx),1)
        ax.plot(xx, (k/Nplotcheby) * yy, 'o')
        ax.plot([-1,1], [(k/Nplotcheby),(k/Nplotcheby)], '-k')

        ax.set_aspect('equal')

    plt.show()

if False:

    print(">> Jacobi evals...")

    alpha = -.5
    beta = -.5
    N = 8

    xx = matrix(linspace(-1,1,50))

    fig, ax = plt.subplots()
    for k in range(1,N):
        p, pd = jacobi_eval(xx, k, alpha, beta)
        ax.plot(xx, p[:,0], '-')

    print(">> Jacobi roots...")

    for k in range(1,N):
        x = jacobi_roots(k, alpha, beta)
        ax.plot(x, zeros(len(x),1), 'o')

    plt.show()

if True:
    fig, ax = plt.subplots()
    xx = matrix(linspace(-1,1,50))
    ax.plot(xx, polyvalv(mon(2),xx), '-')
    plt.show()
