#!/usr/bin/python3

import plotly.offline as py
import plotly.graph_objs as go
import numpy as np
from chebyshev import *
from poly import *
from quad import *

traces = []

alpha = 0
beta = 0
N = 2
M = 100

ab = r_jacobi(N, 0, 0)
XW = xw_lobatto(N-2, ab, -1, 1)

xx = np.array(XW[:,0],dtype=np.float32)
trace = go.Scatter(x=xx, y=np.zeros_like(xx), mode='markers')
traces.append(trace)

L = lagrange(xx)
xxfine = np.linspace(-1,1,M)
for k in range(N):
    yy = np.array(np.polyval(L[k,:],xxfine),dtype=np.float32)
    trace = go.Scatter(x=xxfine, y=yy, mode='lines', name="P" + str(k))
    traces.append(trace)


layout = go.Layout(title='Lagrange polynomials on Legendre-Gauss-Lobatto nodes N=' + str(N))
fig = go.Figure(data=traces, layout=layout)

py.plot(fig, filename='lglpoly.html')
