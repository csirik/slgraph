#!/usr/bin/python3

import plotly.offline as py
import plotly.graph_objs as go
from plotly import tools

import random
import numpy as np
from chebyshev import *
from poly import *
from slgraph import *
from fe1 import *


############################################
# Spectrum
############################################
np.set_printoptions(precision=4, linewidth=120,suppress=True)

N = 1
K = 10

#print(lobatto(5))

#exit()

# generate Legendre-Gauss-Lobatto nodes
#ab = r_jacobi(K, 0, 0)
#XW = xw_lobatto(K-2, ab, -1.0, 1.0)

#shapefuns = lagrange(XW[:,0])
#shapefuns = lobatto(K)

#print(shapefuns)

#print(XW[:,0])

slg = SLGraph()
#slg.create_from_edge_list( [(1,2),(2,3),(2,4)],
#                           edge_lengths=[1.0,1.0,1.0], nelperedge=N1,
#                           node_bc={5:SLGraph.DIRICHLET, 6:SLGraph.DIRICHLET},
#                           xy={1:(0,0),2:(1,0),3:(2,1),4:(2,-1)})

# slg.create_from_edge_list( [(1,2),(2,3),(2,4),(1,5),(1,6)],
#                            edge_lengths=[1.0,1.0,1.0,1.0,1.0,1.0], nelperedge=N1,
#                            xy={1:(0,0),2:(1,0),3:(2,1),4:(2,-1),5:(-1,1),6:(-1,-1)})


slg.create_from_edge_list( [(1,2),(2,3),(2,4)],
                          edge_lengths=[np.pi,np.pi,np.pi], nelperedge=N, nsubels=K,
                          node_bc={1:SLGraph.NEUMANN, 3:SLGraph.NEUMANN},
                          xy={1:(0,0),2:(1,0),3:(2,1),4:(2,-1)})
# slg.create_from_edge_list( [(1,2)],
#                           edge_lengths=[np.pi], nelperedge=N, nsubels=K,
#                           node_bc={1:SLGraph.NEUMANN, 2:SLGraph.NEUMANN},
#                           xy={1:(0,0),2:(1,0)})

lam,ev = fe_lgl_eig(slg,K)

if False:
    xx = np.linspace(1,len(lam),len(lam))
    yy = [x**2 for x in np.linspace(0,len(lam)-1,len(lam))]
    zz = [float(l) for l in lam]

    trace = go.Scatter(x = xx, y = zz, mode = 'markers')
    trace2 = go.Scatter(x = xx, y = yy)

    traces = [trace, trace2]
else:
    lenmax = int(0.6*len(lam))
    xx = np.arange(lenmax)
    yy = [np.sqrt(lam[i])for i in range(lenmax)]
    exact = np.zeros(lenmax)
    for i in range(int(lenmax/3)):
        exact[3*i] = i
        exact[3*i+1] = i +0.5
        exact[3*i+2] = i + 0.5

    error = [abs(exact[i]-yy[i]) for i in range(lenmax)]

    trace = go.Scatter(x = xx, y = yy, mode = 'markers')
    trace2 = go.Scatter(x = xx, y = exact, mode = 'markers')
    trace3 = go.Scatter(x = xx, y = error, mode = 'markers')

    traces = [trace3]


layout = go.Layout(
    xaxis=dict(
    tickformat=":04,2f",
    autorange=True
    ),
    yaxis=dict(
    tickformat=":04,2f",
    autorange=True
    )
)
fig = go.Figure(data=traces, layout=layout)
fig['layout'].update(title='Eigenvalues, ndofs=' + str(slg.ndofs))

py.plot(fig, filename='slgraph_spectrum.html')


