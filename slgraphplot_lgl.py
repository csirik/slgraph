#!/usr/bin/python3

import plotly.offline as py
import plotly.graph_objs as go
from plotly import tools

import random
import numpy as np
from chebyshev import *
from poly import *
from slgraph import *
from fe1 import *

def xyedges(slg):
    xx = []
    yy = []

    for edge in slg.G.edges():
        e0 = slg.xy[edge[0]]
        e1 = slg.xy[edge[1]]
        
        xx.append(e0[0])
        yy.append(e0[1])
        xx.append(e1[0])
        yy.append(e1[1])

    return xx,yy

def plot_slg(sgl): 
    traces = []

    for (el,er,i) in slg.G.edges_iter(data='i'):
        xx = [slg.xy[el][0],slg.xy[er][0]]
        yy = [slg.xy[el][1],slg.xy[er][1]]
        edge = go.Scatter3d( x=xx, y=yy, z=[0,0],
                             mode='lines',
                             line=go.Line(
                                 color='rgb(0,0,0)',
                                 width=5
                             ))
        traces.append(edge)
    
    xx, yy = xyedges(slg)
    zz = np.zeros_like(xx)

    #print(xx)
    #print(yy)
    
    #tredges = go.Scatter3d( x=xx, y=yy, z=zz,
    #                        mode='lines',
    #                        line=go.Line(
    #                            color='rgb(0,0,0)',
    #                            width=5
    #                        ))

    #traces.append(tredges)

    trvertices = go.Scatter3d( x=xx, y=yy, z=zz,
                               mode='markers',
                               marker=go.Marker(
                                   color='rgb(0,0,0)',
                                   size=3,
                                   symbol='circle'
                               ))

    traces.append(trvertices)
    return traces

def plot_eigfun(slg, ev, K):
    random.seed()
    rand_color = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]
    traces = []
    for (el,er,i) in slg.G.edges_iter(data='i'):
        edgep = [np.array(slg.xy[el]), np.array(slg.xy[er])]
        xx = []
        yy = []
        zz = []


        for elem in range(slg.nelperedge):
            elemp = [edgep[0] + (elem/slg.nelperedge)* (edgep[1] - edgep[0]),
                     edgep[0] + ((elem+1) / slg.nelperedge) * (edgep[1] - edgep[0])]

            for t in np.linspace(-1.0, 1.0, 100):
                subelp = elemp[0] + 0.5*(t + 1) * (elemp[1] - elemp[0])
                xx.append(float(subelp[0]))
                yy.append(float(subelp[1]))

                accum = 0

                for subelem in range(slg.nsubels):
                    accum += ev[slg.elems[i,elem,subelem],K] * polyvalv( slg.shapefuns[subelem,:], [t])

                zz.append(float(accum))


        trfun = go.Scatter3d( x=xx, y=yy, z=zz,
                              mode='lines',connectgaps=True,
                              line=go.Line(
                                  color='#%02X%02X%02X' % (rand_color[0],rand_color[1],rand_color[2]),
                                  width=10
                              ),
                              name='eigenfunction'+str(K))
        traces.append(trfun)

    return traces

############################################
# Demo
############################################
np.set_printoptions(precision=4, linewidth=120,suppress=True)

N1 = 100
K = 2

# generate Legendre-Gauss-Lobatto nodes
ab = r_jacobi(K, 0, 0)
XW = xw_lobatto(K-2, ab, -1.0, 1.0)

shapefuns = lagrange(XW[:,0])

#print(shapefuns)

#print(XW[:,0])

slg = SLGraph()
#slg.create_from_edge_list( [(1,2),(2,3),(2,4)],
#                           edge_lengths=[1.0,1.0,1.0], nelperedge=N1,
#                           node_bc={5:SLGraph.DIRICHLET, 6:SLGraph.DIRICHLET},
#                           xy={1:(0,0),2:(1,0),3:(2,1),4:(2,-1)})

# slg.create_from_edge_list( [(1,2),(2,3),(2,4),(1,5),(1,6)],
#                            edge_lengths=[1.0,1.0,1.0,1.0,1.0,1.0], nelperedge=N1,
#                            xy={1:(0,0),2:(1,0),3:(2,1),4:(2,-1),5:(-1,1),6:(-1,-1)})


# slg.create_from_edge_list( [(1,2),(2,3),(2,4)],
#                           edge_lengths=[1.0,1.0,1.0], nelperedge=N1, nsubels=K, subelnodes=XW[:,0], shapefuns=shapefuns,
#                           node_bc={1:SLGraph.NEUMANN, 3:SLGraph.NEUMANN},
#                          xy={1:(0,0),2:(1,0),3:(2,1),4:(2,-1)})
slg.create_from_edge_list( [(1,2)],
                          edge_lengths=[1.0], nelperedge=N1, nsubels=K, subelnodes=XW[:,0], shapefuns=shapefuns,
                          node_bc={1:SLGraph.NEUMANN, 2:SLGraph.NEUMANN},
                         xy={1:(0,0),2:(1,0)})

lam,ev = fe_lgl_eig(slg,K)



delta = np.zeros((slg.ndofs, 1))
delta[1] = 1.0


#print(ev)


traces = []
for t in plot_slg(slg):
    traces.append(t)
for t in plot_eigfun(slg, ev, 3):
    traces.append(t)

fig = go.Figure(data=traces)

py.plot(fig, filename='slgraph_lgl.html')

exit()





Kr = 2
Kc = 3

titles = ['Eigenfunction ' + str(i) for i in range(1, Kr*Kc+1)]

specs = []
for dummy in range(Kr):
    specs.append([ {'is_3d': True} for dummy in range(Kc) ])

fig = tools.make_subplots(rows=Kr, cols=Kc,
                          specs=specs )

for kr in range(1,Kr+1):
    for kc in range(1,Kc+1):
        funtraces = plot_eigfun(slg, ev, kc+(kr-1)*Kc - 1)
        for trace in funtraces:
            fig.append_trace(trace, kr, kc)

        traces = plot_slg(slg)
        for trace in traces:
            fig.append_trace(trace, kr, kc)
            

fig['layout'].update(title='Eigenfunctions')

#layout = go.Layout(title='SLGraph', margin=dict(l=0,r=0,b=0,t=65))
#fig = go.Figure(data=traces, layout=layout)

py.plot(fig, filename='slgraph_lgl.html')
