#!/usr/bin/python3

from poly import *
import numpy as np

def xw_cheby_gauss(n):
    x = cheby1(n)
    w = (pi/n) * ones(n,1)
    return row_join(x,w)
    
# % GAUSS Gauss quadrature rule.
# %
# %    Given a weight function w encoded by the nx2 array ab of the
# %    first n recurrence coefficients for the associated orthogonal
# %    polynomials, the first column of ab containing the n alpha-
# %    coefficients and the second column the n beta-coefficients,
# %    the call xw=GAUSS(n,ab) generates the nodes and weights xw of
# %    the n-point Gauss quadrature rule for the weight function w.
# %    The nodes, in increasing order, are stored in the first
# %    column, the n corresponding weights in the second column, of
# %    the nx2 array xw.
# %
def xw_gauss(N,ab):
    N0 = ab.rows
    if N0 < N:
        print('input array ab too short')
        return []

    J = np.zeros((N,N))

    for n in range(N):
        J[n,n] = float(ab[n,0])
    for n in range(1,N):
        J[n,n-1] = float(sqrt(ab[n,1]))
        J[n-1,n] = J[n,n-1]


    evals, evecs = np.linalg.eig(J)


    evalss = np.sort(evals)
    evecss = evecs[:,evals.argsort()]

    return np.vstack((evalss, float(ab[0,1])*np.square(evecss[0,:]))).transpose()

# % LOBATTO Gauss-Lobatto quadrature rule.
# %
# %    Given a weight function w encoded by the (n+2)x2 array ab of
# %    the first n+2 recurrence coefficients for the associated
# %    orthogonal polynomials, the first column of ab containing the
# %    n+2 alpha-coefficients and the second column the n+2 beta-
# %    coefficients, the call xw=LOBATTO(n,ab,endl,endr) generates
# %    the nodes and weights xw of the (n+2)-point Gauss-Lobatto
# %    quadrature rule for the weight function w having two
# %    prescribed nodes endl, endr (typically the left and right end
# %    points of the support interval of w, or points to the left
# %    resp. to the right therof). The n+2 nodes, in increasing
# %    order, are stored in the first column, the n+2 corresponding
# %    weights in the second column, of the (n+2)x2 array xw.
# %
# %    For Jacobi weight functions, see also LOBATTO_JACOBI.
# %
def xw_lobatto(N,ab,endl,endr):
    N0 = ab.rows
    if N0 < N+2:
        print('input array ab too short')
        return []
    ab0 = ab
    p0l = 0
    p0r = 0
    p1l = 1
    p1r = 1
    for n in range(N+1):
        pm1l = p0l
        p0l = p1l
        pm1r = p0r
        p0r = p1r
        p1l = (endl-ab0[n,0])*p0l-ab0[n,1]*pm1l
        p1r = (endr-ab0[n,0])*p0r-ab0[n,1]*pm1r

    det = p1l*p0r-p1r*p0l

    ab0[N+1,:] = matrix([[(endl*p1l*p0r-endr*p1r*p0l)/det, (endr-endl)*p1l*p1r/det]])

    return xw_gauss( N+2, ab0 )

def xw_quad(P, c, d, XW):
    a = -1.0
    b = 1.0
    X = (c-d)/(a-b) * XW[:,0] + (a*d-b*c)/(a-b)

    Px = np.polyval(P, X)
    return np.dot(Px, XW[:,1]) * (c-d)/(a-b)