#!/usr/bin/python3

import networkx as nx
import numpy as np
import scipy.sparse as sp
from networkx.drawing.nx_agraph import write_dot
import matplotlib.pyplot as plt

# Discretization size
N = 5
# Constant edge length
a = np.pi
# Coefficient of system matrix
h = a/(N-1)
h2inv = 1/(h*h)
# left-right dof indices
lr2i = [0,N-1]
# neighboring left-right dof indices
lrn2i = [1,N-2]
# next-neighboring left-right dof indices
lrnn2i = [2,N-3]

# Is the whole graph a single loop?
single_loop = False


def eig(G, kmax):
    ndofs,indx,nondofs = gen_index(G)
    #print(indx)
    #print(nondofs)
    A = assemble(G,ndofs,indx,nondofs)

#
# Generate index map
#
def gen_index(G):
    global single_loop
    indx = np.zeros([G.size(), N])
    nondofs = []
    ii = 0

    # iterate over all vertices
    for v in G.nodes_iter():
        # iterate over the edge neighborhood
        for (el,er,i) in G.edges_iter(v,data='i'):
            e = (el,er)

            # use common vertex order in edges
            lr = list([(f,g) for (f,g,k) in G.edges(data='i') if k==i][0]).index(v)

            if el == er:
                # loop edge
                if G.degree(v) == 2:
                    # the whole graph is a single loop
                    single_loop = True
                    
                indx[i][0] = ii
                indx[i][N-1] = ii
            else:
                indx[i][lr2i[lr]] = ii

        if G.degree(v) > 1 and not single_loop:
            nondofs.append(ii)
             
        ii += 1
        


    # fill in internal dofs
    for ei in range(G.size()):
        indx[ei][1:N-1] = range(ii,ii+N-2)
        ii += N-2

    return ii,indx,nondofs
                      
#
# Assemble system matrix
#
def assemble(G,ndofs,indx,nondofs):
    print('>> Assemble')

    A = np.zeros( [ndofs, ndofs] )
    
    # interior dofs
    for (el,er,i) in G.edges_iter(data='i'):
        e = (el,er)

        for s in range(1,N-1):
            A[indx[i,s],indx[i,s]] = 2.
            A[indx[i,s],indx[i,s-1]] = -1.
            A[indx[i,s],indx[i,s+1]] = -1.      

    # nodal dofs:
    # iterate over all vertices
    for v in G.nodes_iter():
        d = G.degree(v)
        if d == 1:
            # use common vertex order in edges
            f,g,i = [(f,g,k) for (f,g,k) in G.edges(data='i') if f==v or g==v][0]
            lr = [f,g].index(v)
            
            # boundary vertex: homogeneous Neumann
            A[indx[i,lr2i[lr]],indx[i,lr2i[lr]]] = 2.
            A[indx[i,lr2i[lr]],indx[i,lrn2i[lr]]] = -2.
        else:
            # iterate over the edge neighborhood
            for (el,er,i) in G.edges_iter(v,data='i'):
                e = (el,er)

                # use common vertex order in edges
                lr = list([(f,g) for (f,g,k) in G.edges(data='i') if k==i][0]).index(v)


                # Eliminated Kirchoff condition
                #A[indx[i,lrn2i[lr]],indx[i,lrn2i[lr]]] = 2.-1/d
                #A[indx[i,lrn2i[lr]],indx[i,lrnn2i[lr]]] = -1
                
                A[indx[i,lrn2i[lr]],indx[i,lrn2i[lr]]] = 2. - 4./(3.*d)
                A[indx[i,lrn2i[lr]],indx[i,lrnn2i[lr]]] =  1./(3.*d) - 1.
                
                # iterate over the other edges to eliminated the nodal dof
                for (fl,fr,j) in G.edges_iter(v,data='i'):
                    if i != j:
                        f = (fl,fr)
                        # use common vertex order in edges
                        lr2 = list([(f,g) for (f,g,k) in G.edges(data='i') if k==i][0]).index(v)
                        
                        if el == er:
                            # loop edge
                            A[indx[i,1],indx[j,lrn2i[lr2]]] = -1./d
                            A[indx[i,N-2],indx[j,lrn2i[lr2]]] = -1./d
                        else:
                            A[indx[i,lrn2i[lr]],indx[j,lrn2i[lr2]]] =  4./(3.*d)
                            A[indx[i,lrn2i[lr]],indx[j,lrnn2i[lr2]]] = -1./(3.*d)



    dofs = list(set(range(ndofs)) - set(nondofs))
    A = A[np.ix_(dofs,dofs)]
    #print(dofs)
    np.set_printoptions(precision=2)
    print(A)
    lam, v = np.linalg.eig(A)
    lam = np.sort(lam)

    fig, ax = plt.subplots()
    k = range(len(lam))
    ax.plot(k, lam, 'ro')
    plt.show()



print('====================================')
print('slgraph -- Sturm-Liouville on Graphs')
print('')
print('networkx version: ' + str(nx.__version__))
print('numpy version: ' + str(np.__version__))
print('====================================')

G = nx.MultiGraph()
#G.add_edges_from([(1,2), (1,2), (2,3), (1,1)])

edge_list = [(1,2), (1,2), (2,3), (1,1)]

edge_list = [(1,1), (1,2)]

edge_list = [(1,2), (2,3), (2,4), (5,1), (6,1)]

edge_list = [(1,2), (2,3), (2,3)]

edge_list = [(1,2), (2,3)]
#edge_list = [(1,2)]

edge_list = [ (*e,i) for i,e in enumerate(edge_list)]
G.add_weighted_edges_from(edge_list,'i')



eig(G, 50)


write_dot(G,'multi.dot')
