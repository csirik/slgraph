#!/usr/bin/python3

import platform
import networkx as nx

import scipy as scipy
import scipy.linalg as sp

from networkx.drawing.nx_agraph import write_dot
import matplotlib.pyplot as plt

from slgraph import *

def fe1_eig(slg):
    print('>> Assemble lowest-order FE')
    A,M = fe1_assemble(slg)
    scipy.set_printoptions(precision=2)
   # print(A)
   # print(M)
    print('>> Eig')
    lam,ev = sp.eigh(A,b=M)
    #lam,ev = sp.eigh(A)


#    lam = sp.eigvals(A)
    lam = scipy.sort(lam)

    return lam,ev


                      
#
# Assemble system matrix for linear finite elements
#
def fe1_assemble(slg):
    # Stiffness matrix
    A = scipy.zeros( [slg.ndofs, slg.ndofs] )
    # Mass matrix
    M = scipy.zeros( [slg.ndofs, slg.ndofs] )

    # iterate over all elements
    for (edgel,edger,i) in slg.G.edges_iter(data='i'):
        edge = (edgel,edger)

        he = slg.edge_lengths[i]/slg.nelperedge
        heinv = slg.nelperedge/slg.edge_lengths[i]
        
        for elem in range(slg.nelperedge):
            for ei in range(2):
                if slg.elems[i,elem,ei] != slg.NONDOF:
                    for ej in range(2):
                        if slg.elems[i,elem,ej] != slg.NONDOF:
                            if ei == ej:
                                A[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += heinv
                                M[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += he/3.
                            else:
                                A[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] -= heinv
                                M[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += he/6.
    return A,M
    
    #dofs = list(set(range(ndofs)) - set(nondofs))
    #A = A[np.ix_(dofs,dofs)]
    #print(dofs)
    #np.set_printoptions(precision=2)
    #print(A)




print('====================================')
print('slgraph -- Sturm-Liouville on Graphs')
print('')
print('Python version: ' + str(platform.python_version()))
print('networkx version: ' + str(nx.__version__))
print('scipy version: ' + str(scipy.__version__))
print('====================================')


#G.add_edges_from([(1,2), (1,2), (2,3), (1,1)])

edge_list = [(1,2), (1,2), (2,3), (1,1)]

edge_list = [(1,1), (1,2)]

if False:
    # >-<
    N1 = 100
    slg1 = SLGraph()
    slg1.create_from_edge_list( [(1,2), (2,3), (2,4), (5,1), (6,1)],
                                edge_lengths=[2.0, 1.0, 1.0, 1.0, 1.0], nelperedge=N1,
                                node_bc={5:SLGraph.DIRICHLET, 6:SLGraph.DIRICHLET})
    lam1,ev1 = fe1_eig(slg1)
    

    # -<>-
    N2 = 125
    slg2 = SLGraph()
    slg2.create_from_edge_list( [(1,2), (2,3), (2,3), (3,4)],
                                edge_lengths=[1.0, 2.0, 2.0, 1.0], nelperedge=N2,
                                node_bc={4:SLGraph.DIRICHLET})
    lam2,ev1 = fe1_eig(slg2)
    
    kmax = 300


if True:

    N1 = 200
    slg1 = SLGraph()
    slg1.create_from_edge_list( [(1,2)], edge_lengths=[scipy.pi], nelperedge=N1,
                                node_bc={1:SLGraph.NEUMANN, 2:SLGraph.NEUMANN})
#    slg1.create_from_edge_list( [(1,2)], edge_lengths=[1.0], nelperedge=N1)
    lam1,ev1 = fe1_eig(slg1)
    
    N2 = 100
    slg2 = SLGraph()
    slg2.create_from_edge_list( [(1,2),(2,3)], edge_lengths=[0.5*scipy.pi,0.5*scipy.pi], nelperedge=N2,
                                node_bc={1:SLGraph.NEUMANN, 3:SLGraph.NEUMANN})
    lam2,ev2 = fe1_eig(slg2)

kmax = 100

#print(ev1[0,:])

fig, ax = plt.subplots()
if False:
    k = range(kmax)
    ax.plot(k, lam1[0:kmax], 'ro')
    ax.plot(k, lam2[0:kmax], 'bo')
else:
    k1 = range(len(lam1))
    k2 = range(len(lam2))
    ax.plot(k1, lam1, 'ro')
    ax.plot(k2, lam2, 'bo')
    #ax.plot(k1, [4*((N1-1)*(N1-1))/(scipy.pi*scipy.pi) * scipy.sin(0.5*scipy.pi*l/(N1-1))**2 for l in k1], 'g-')
    ax.plot(k1, [l*l for l in k1], 'y-')

fig, ax = plt.subplots()
#for k in range(len(lam2)):
for k in range(5):
#    ev = scipy.concatenate(([0],ev1[:,k],[0]))
#    ev = scipy.concatenate((ev1[:,k],[0]))
    ev = ev1[:,k]
    ax.plot(scipy.linspace(0,1,N1+1), ev, '-o')

#ax.plot(k, [l*l * 0.35 for l in k], 'yo')

#ax.set_aspect(2/kmax)


plt.show()


#write_dot(G,'multi.dot')
