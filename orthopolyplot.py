#!/usr/bin/python3

import plotly.offline as py
import plotly.graph_objs as go
import numpy as np
from chebyshev import *
from poly import *

traces = []

alpha = 0
beta = 0
N = 10
M = 100

xx = matrix(np.linspace(-1,1,M))
xxf = np.linspace(-1,1,M)

for k in range(1,N):
    p, pd = jacobi_eval(xx, k, alpha, beta)
    yy = np.array(p[:,0],dtype=np.float32)
    trace = go.Scatter(x=xxf, y=yy, mode='lines', name="P"+str(k))
    traces.append(trace)
    
for k in range(1,N):
        x = jacobi_roots(k, alpha, beta)
        xx = np.array(x,dtype=np.float32)
        trace = go.Scatter(x=xx, y=np.zeros_like(xx), mode='markers', name="X"+str(k))
        traces.append(trace)


layout = go.Layout(title='Jacobi polynomials and roots: alpha=' + str(alpha) + ' beta=' + str(beta))
fig = go.Figure(data=traces, layout=layout)

py.plot(fig, filename='orthopoly.html')
