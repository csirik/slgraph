#!/usr/bin/python3

import platform

import scipy as scipy
import scipy.linalg as sp

from chebyshev import *
from poly import *
