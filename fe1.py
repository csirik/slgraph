#!/usr/bin/python3

import platform
import networkx as nx

import scipy as scipy
import scipy.linalg as sp

from quad import *

from networkx.drawing.nx_agraph import write_dot
import matplotlib.pyplot as plt

from slgraph import *

def fe1_eig(slg):
    print('>> Assemble lowest-order FE')
    A,M = fe1_assemble(slg)
    scipy.set_printoptions(precision=2)
   # print(A)
   # print(M)
    print('>> Eig')
    lam,ev = sp.eigh(A,b=M)
    #lam,ev = sp.eigh(A)


#    lam = sp.eigvals(A)
    lam = scipy.sort(lam)

    return lam,ev

def fe_lgl_eig(slg, K):
    A,M = fe_lgl_assemble(slg,K)
    scipy.set_printoptions(precision=2)
    print(A)
    print(M)
    print('>> Eig')
    lam,ev = sp.eigh(A,b=M)
    #lam,ev = sp.eigh(A)


#    lam = sp.eigvals(A)
    lam = scipy.sort(lam)

    return lam,ev

                      
#
# Assemble system matrix for linear finite elements
#
def fe1_assemble(slg):
    # Stiffness matrix
    A = scipy.zeros( [slg.ndofs, slg.ndofs] )
    # Mass matrix
    M = scipy.zeros( [slg.ndofs, slg.ndofs] )

    # iterate over all elements
    for (edgel,edger,i) in slg.G.edges_iter(data='i'):
        edge = (edgel,edger)

        he = slg.edge_lengths[i]/slg.nelperedge
        heinv = slg.nelperedge/slg.edge_lengths[i]
        
        for elem in range(slg.nelperedge):
            for ei in range(2):
                if slg.elems[i,elem,ei] != slg.NONDOF:
                    for ej in range(2):
                        if slg.elems[i,elem,ej] != slg.NONDOF:
                            if ei == ej:
                                A[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += heinv
                                M[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += he/3.
                            else:
                                A[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] -= heinv
                                M[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += he/6.
    return A,M
    
    #dofs = list(set(range(ndofs)) - set(nondofs))
    #A = A[np.ix_(dofs,dofs)]
    #print(dofs)
    #np.set_printoptions(precision=2)
    #print(A)


#
# Generate Lobatto polinomials
#
def lobatto(K):
    Lob = np.zeros((K,K))

    Lob[0, -1] = 0.5
    Lob[0, -2] = -0.5
    Lob[1, -1] = 0.5
    Lob[1, -2] = 0.5

    if K > 2:
        ab = r_jacobi(K-1, 0, 0)
        Leg = ab_to_poly(ab)

        for i in range(2,K):
            Leg1 = float(1/polyval(Leg[i-1,:],1))
            Legpri =  polypri(Leg[i-1,:])
            Lob[i,:] = Legpri
            Lob[i,-1] -= polyvalv(Legpri,[-1])
            Lob[i,:] = [l*sqrt((2*i-1)/2)*Leg1 for l in Lob[i,:]]

    return Lob

#
# Assemble local stiffness and mass matrix for Lagrange basis on Legendre-Gauss-Lobatto nodes
#
def lgl_stiff_mass(K,lumped=True):
    ab = r_jacobi(K, 0, 0)
    XW = xw_lobatto(K-2, ab, -1, 1)

    # higher order quadrature for evalation
    abh = r_jacobi(K, 0, 0)
    XWh = xw_lobatto(K-2, abh, -1, 1)

    Aloc = np.zeros((K, K))
    Mloc = np.zeros((K, K))


    L = lagrange(XW[:,0])
    #L = lagrange(np.linspace(-1,1,K))
    #L = lagrange(cheby2(K))
    #L = lobatto(K)
    #print(L)

    for i in range(K):
        for j in range(K):
            pq = conv(L[i,:], L[j,:])
            dpdq = conv(polyder(L[i, :]), polyder(L[j, :]))

            Aloc[i,j] = xw_quad(dpdq, -1, 1, XWh)
            Mloc[i,j] = xw_quad(pq, -1, 1, XWh)

    return Aloc, Mloc, XW[:,0], L

#
# Assemble system matrix for spectral finite elements
#
def fe_lgl_assemble(slg, K):
    print('>> Assemble spectral FE, K=' + str(K) + ' nelperedge=' + str(slg.nelperedge) + ' ndofs=' + str(slg.ndofs))

    print('>>>> Generating local matrices')
    # Local matrices
    Aloc, Mloc, slg.subelnodes, slg.shapefuns = lgl_stiff_mass(K)
    print(Aloc)
    print(Mloc)

    print('>>>> Dense assembly')

    # Stiffness matrix
    A = scipy.zeros( [slg.ndofs, slg.ndofs] )
    # Mass matrix
    M = scipy.zeros( [slg.ndofs, slg.ndofs] )

    # iterate over all elements
    for (edgel,edger,i) in slg.G.edges_iter(data='i'):
        edge = (edgel,edger)

        he = slg.edge_lengths[i]/float(slg.nelperedge)
        heinv = float(slg.nelperedge)/slg.edge_lengths[i]

        for elem in range(slg.nelperedge):
            for ei in range(slg.nsubels):
                if slg.elems[i,elem,ei] != slg.NONDOF:
                    for ej in range(slg.nsubels):
                        if slg.elems[i,elem,ej] != slg.NONDOF:
                            A[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += 2*heinv*Aloc[ei,ej]
                            M[slg.elems[i,elem,ei],slg.elems[i,elem,ej]] += 0.5*he*Mloc[ei,ej]

    return A,M