
import networkx as nx
import scipy as scipy

class SLGraph(object):
    #
    # Boundary condition types
    #
    DIRICHLET = 0
    NEUMANN = 1

    # Not a degree of freedom indicator
    NONDOF = -1

    # Internal boundary condtion
    KIRCHOFF = 0

    def __init__(self):
        #
        # Metric graph
        #
        self.G = nx.MultiGraph()
        self.single_loop = False
        self.edge_list = []
        self.edge_lengths = []
        self.node_bc = {} # Node->Boundary type dictionary
        self.xy = [] # Node coordinates

        #
        # Finite element discretization
        #
        self.elems = [] # Element array
        self.nelperedge = 0 # NUumber of elements per edge
        self.nsubels = 2 # Number of subelements per element (i.e. 2 for linear)
        self.nondofs = [] # Dirichlet nodes
        self.ndofs = 0 # Number of Degrees of Freedom
        self.subelnodes = [-1.0, 1.0] # Subelement nodes on the reference element [-1,1]
        self.shapefuns = [[-0.5,0.5],[0.5,0.5]] # Shape functions

    def create_from_edge_list(self,edge_list, **options):
        self.edge_lengths = options.get("edge_lengths")
        self.node_bc = options.get("node_bc")
        self.nelperedge = options.get("nelperedge")
        self.nsubels = options.get("nsubels")
        self.subelnodes = options.get("subelnodes")
        self.shapefuns = options.get("shapefuns")
        self.xy = options.get("xy")
        
        # concatenate index and add edges
        self.edge_list = [ (*e,i) for i,e in enumerate(edge_list)]
        self.G.add_weighted_edges_from(self.edge_list,'i')

        self.gen_index()
        #print(self.elems)


    #
    # Generate index map
    #
    def gen_index(self):
        # left-right dof indices
        lr2i = [0,self.nelperedge-1]
        lr2s = [0,self.nsubels-1]

        self.elems = scipy.zeros([self.G.size(), self.nelperedge, self.nsubels])
        self.nondofs = []
        ii = 0

        # iterate over all vertices
        for v in self.G.nodes_iter():
            # iterate over the edge neighborhood if dof
            if self.node_bc is not None and v in self.node_bc \
            and self.node_bc[v] == self.DIRICHLET:
                # DIRICHLET boundary
                assert(self.G.degree(v)==1)
                f,g,i = [(f,g,k) for (f,g,k) in self.G.edges(data='i') if f==v or g==v][0]
                lr = [f,g].index(v)
                # mark as nondof
                self.elems[i,lr2i[lr],lr2s[lr]] = self.NONDOF
            else:
                # NEUMANN boundary or internal node
                for (el,er,i) in self.G.edges_iter(v,data='i'):
                    e = (el,er)
                    
                    # use common vertex order in edges
                    lr = list([(f,g) for (f,g,k) in self.G.edges(data='i') if k==i][0]).index(v)
                    
                    if el == er:
                        # loop edge
                        if self.G.degree(v) == 2:
                            # the whole graph is a single loop
                            self.single_loop = True
                        
                        self.elems[i,0,0] = ii
                        self.elems[i,self.nelperedge-1,self.nsubels-1] = ii
                    else:
                        # normal edge
                        self.elems[i,lr2i[lr],lr2s[lr]] = ii
                        
                    #if G.degree(v) > 1 and not single_loop:
                    #    nondofs.append(ii)
                        
                    # increment index for every node
                ii += 1

        # fill in internal dofs if any
        if self.nelperedge > 1 or self.nsubels > 2:
            # loop over all edges
            for (el,er,i) in self.G.edges_iter(data='i'):
                # loop over all elements on an edge
                for ei in range(self.nelperedge):
                    # loop over all subelements of an element
                    for si in range(self.nsubels):
                        if self.nelperedge == 1:
                            # Single element per edge
                            if si != 0 and si != self.nsubels-1:
                                self.elems[i,ei,si] = ii
                                ii += 1
                        else:
                            # Multiple elements per edge
                            if ei == 0:
                                # "leftmost" element on an edge
                                if si != 0:
                                    # internal subelement dof
                                    self.elems[i,ei,si] = ii
                                    ii += 1
                            elif ei == self.nelperedge-1:
                                # rightmost element
                                if si == 0:
                                    # connect leftmost subelement dof with the previous
                                    # elements rightmost subelement dof
                                    self.elems[i,ei,0] = self.elems[i,ei-1,self.nsubels-1]
                                elif si != self.nsubels-1:
                                    self.elems[i,ei,si] = ii
                                    ii += 1
                            else:
                                # generic element on an edge
                                if si == 0:
                                    # connect leftmost subelement dof with the previous
                                    # elements rightmost subelement dof
                                    self.elems[i,ei,0] = self.elems[i,ei-1,self.nsubels-1]
                                else:
                                    self.elems[i,ei,si] = ii
                                    ii += 1


        self.ndofs = ii 
